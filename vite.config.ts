import { defineConfig } from "vite";
import * as path from "path";
import solidPlugin from "vite-plugin-solid";

export default defineConfig({
    resolve: {
        alias: {
            '/@lib': path.resolve(__dirname, './src/components'),
            '/@': path.resolve(__dirname, './src')
        }
    },
    plugins: [solidPlugin()],
    build: {
        target: "esnext",
        polyfillDynamicImport: false,
    }
});
