# colorer

Play around with hsla color!

Try it online: https://colorer.vercel.app/

## Usage

``` bash
git clone
yarn && yarn dev
```

## Tech

Build with [Solid](https://www.solidjs.com), [tailwindcss](https://tailwindcss.com) and lots of 💗
