import { createSignal, createEffect, createContext, useContext } from "solid-js";

const HslaContext = createContext();

export function HslaProvider(props) {
    const defaultHsla = { h: 1, s: 50, l: 50, a: 1 };
    const [hsla, setHsla] = createSignal(props.hsla || defaultHsla),
        store = [
            hsla,
            {
                setHsla(value) {
                    setHsla(value)
                },
                setH(value) {
                    setHsla({ ...hsla(), h: value })
                },
                setS(value) {
                    setHsla({ ...hsla(), s: value })
                },
                setL(value) {
                    setHsla({ ...hsla(), l: value })
                },
                setA(value) {
                    setHsla({ ...hsla(), a: value })
                }
            }
        ];

    return (
        <HslaContext.Provider value={store}>
            {props.children}
        </HslaContext.Provider>
    );
}

export function useHslaContext() {
    const context = useContext(HslaContext);
    const storageName = "hsla"
    const [hsla, { setHsla, ...rest }] = context;

    if (window.localStorage) {
        const storedHsla = window.localStorage.getItem(storageName);
        if (storedHsla != null) {
            setHsla(JSON.parse(storedHsla))
        }

        createEffect(() => {
            window.localStorage.setItem(storageName, JSON.stringify(hsla()));
        });
    }

    return context;
}
