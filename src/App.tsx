import { HslaProvider } from "/@/hsla-context";
import Colorer from "/@lib/colorer";

const startHsla = { h: 1, s: 50, l: 50, a: 1 };

export default () =>
    <HslaProvider hsla={startHsla}>
        <header>
            <h1>colorer</h1>
        </header>
        <main>
            <Colorer />
        </main>
    </HslaProvider>
