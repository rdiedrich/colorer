import { mergeProps } from "solid-js";

type ColorInputProps = {
    label: string;
    min: number;
    max: number;
    value: number;
    onInput: (a: number) => void;
    step?: number
}

function ColorInput(props: ColorInputProps) {
    const options = mergeProps({ step: 1 }, props);

    const setValue = (e: InputEvent) => {
        let eventTarget = e.currentTarget as HTMLInputElement;
        options.onInput(parseFloat(eventTarget.value));
    }

    return (
        <div class="color-input">
            <label>
                <span>{options.label}</span>
                <input type="number"
                    min={options.min} max={options.max} step={options.step}
                    value={options.value} onInput={setValue} />
                <input type="range"
                    min={options.min} max={options.max} step={options.step}
                    value={options.value} onInput={setValue} />
            </label>
        </div>
    )
}

export default ColorInput;
